/**
 * Fonction asynchronne fetchJSON
 * qui permet quand la promesse est résolue
 * de recuperer des données JSON
 * @param {*} url une adresse 
 * @returns une promesse (ou les données si résolue)
 */
 async function fetchJSON(url) {
    let reponse = await fetch(url);
    let data = await reponse.json();
    return data;
}