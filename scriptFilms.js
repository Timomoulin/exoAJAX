//Variables globales
let inputRecherche = document.querySelector("#recherche");
let titreRecherche ;

//Fonctions
function affiche(desFilms) {
    let divFilms = document.querySelector("#films");
    let chaineHTML = "";
    for (const unFilm of desFilms) {
        chaineHTML += (`<div class="card bg-secondary border-warning text-light" style="width: 18rem;">
    
    <img src="${unFilm.Poster}" class="card-img-top" alt="...">
    <div class="card-body">
      <h5 class="card-title">${unFilm.Title} (${unFilm.Year})</h5>
      <p class="card-text"></p>
    </div>
  </div>`)
    }
    divFilms.innerHTML = chaineHTML;
}


//Instructions
inputRecherche.addEventListener("keyup", async function () {
    titreRecherche = inputRecherche.value;
  //@TODO Reucperer les films rechercher est les afficher (url = http://www.omdbapi.com/?s=[titre]&apikey=22b0b810)

})