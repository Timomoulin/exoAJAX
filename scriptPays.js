
let divPays=document.querySelector("#divPays");
let inputRecherche = document.querySelector("#recherche");

function affiche(lesPays)
{
    
    let chaine="";
    for(let unPays of lesPays)
    {
        chaine+=`<div class="card my-2" style="width: 18rem;">
        <img class="card-img-top" src="${unPays.flag}" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">${unPays.name}</h5>
          <p class="card-text">Region : ${unPays.region}<br> Capital : ${unPays.capital} <br> Population : ${unPays.population} <br></p>
        </div>
      </div>`;
    }
    divPays.innerHTML=chaine;
}

function afficheDataList(lesPays)
{
    let chaine="";
    for(let unPays of lesPays)
    {
        chaine+=`<option value="${unPays.name}">`;
    }
    document.querySelector("#dataListePays").innerHTML=chaine;
}

//TODO recuperer le JSON et invoquer afficheDataList et affiche (url = https://restcountries.com/v2/all)



inputRecherche.addEventListener("keyup", async function () {
    let titreRecherche = inputRecherche.value;
  //TODO Recuperer les pays rechercher est les afficher
})