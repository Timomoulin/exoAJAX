<?php
require_once("model/client.class.php");

class ClientManager{

    private $lePDO;

    public function __construct($unPDO)
    {
        $this->lePDO=$unPDO;
    }

    public function fetchClientByEmailAndMdp($email, $hashMdp)
    {
        try{

       
        $connex=$this->lePDO;
        $sql=$connex->prepare("SELECT * FROM client where email=:email and mdp=:mdp");
        
        $sql->bindParam(":email",$email);
        $sql->bindParam(":mdp",$hashMdp);
        $sql->execute();

        $sql->setFetchMode(PDO::FETCH_CLASS,"Client");
        $resultat=$sql->fetch();
        return $resultat;
    }
    catch(PDOException $e)
    {
        echo $e->getMessage();
    }
    }

   
    public function createClient( $client){
        try {
            $connex=$this->lePDO;
            $sql =$connex->prepare("INSERT INTO client values(null,:prenom,:nom,:email,:mdp,:adresse,:ville,:codePostal)");
            $sql->bindValue(":prenom",$client->getPrenom());
            $sql->bindValue(":nom",$client->getNom());
            $sql->bindValue(":email",$client->getEmail());
            $sql->bindValue(":mdp",$client->getMdp());
            $sql->bindValue(":adresse",$client->getAdresse());
            $sql->bindValue(":ville",$client->getVille());
            $sql->bindValue(":codePostal",$client->getCodePostal());
      
        
            $sql->execute();
           
            return true;

        } catch (PDOException $error) {
            echo $error->getMessage();
            return false;
        }
    }

    function fetchAllClient(){

        try {
            //Pour la co on utilise l'attribut lePDO
            $connex=$this->lePDO;
            $sql =$connex->prepare("SELECT * FROM client ");
            $sql->execute();
            $sql->setFetchMode(PDO::FETCH_CLASS,"Client");
            $resultat = ($sql->fetchAll());
            return $resultat;
    
        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }
}
?>