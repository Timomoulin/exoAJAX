<?php
require("model/clientManager.php");
function etablirCo()
{
    $urlSGBD="localhost";
    $nomBDD="exemple_shop"; // le nom de la BDD
    $loginBDD="root";
    $mdpBDD="";// le mdp est root si on utilise Mamp
    try{
    $connex = new PDO("mysql:host=$urlSGBD;dbname=$nomBDD", "$loginBDD", "$mdpBDD", array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    $connex->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch (PDOException $error) {
        echo "Il y a un problème de co a la BDD verifier que la bdd est presente et les lignes 7 à 10 du fichier bdd.php<br>";
        echo $error->getMessage();
    }
    
    return $connex;
}

$lePDO=etablirCo();

if($_SERVER['REQUEST_METHOD']=="POST")
{
    if(isset($_POST['email'])&&isset($_POST['nom'])&&isset($_POST['prenom'])&&isset($_POST['mdp'])&&isset($_POST['adresse'])&&isset($_POST['ville'])&&isset($_POST['codePostal']))
    {
        $email=filter_var($_POST['email'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $nom=filter_var($_POST['nom'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $prenom=filter_var($_POST['prenom'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $mdp=filter_var($_POST['mdp'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $adresse=filter_var($_POST['adresse'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $ville=filter_var($_POST['ville'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $codePostal=filter_var($_POST['codePostal'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);

        //Debut validation
        //Validation Mail
        if(filter_var($emai,FILTER_VALIDATE_EMAIL)==false)
        {
            echo "Echec de l'ajout : Email non valide";
            exit;
        }

        //Validation du mdp
        //un mdp entre 8 et 20 char qui contient des lettres min et maj et un nombre
        // si vous souhaiter un char special 
        // "#.*^(?=.{8,20})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*W).*$#"
        if (preg_match("#.*^(?=.{8,20})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$#", $mdp)==false){
            echo "Echec de l'ajout : Mdp non valide";
            exit;
        
        } 
        // Hashage du MDP (ne pas hasher avant la validation)
        $mdp=hash("sha256",$mdp);
        //Eventuel validation supplémentaire
        //Fin de la validation
        $clientManager=new ClientManager($lePDO);
        $client=new Client();
        $leClient->hydrate($nom,$prenom,$email,$mdp,$adresse,$ville,$codePostal);
        $ajoutOk=$clientManager->createClient($leClient);
        if($ajoutOk){
            echo "Ajout réussi";
        }
        else{
            echo "Echec de l'ajout";
        }
}
}
elseif($_SERVER['REQUEST_METHOD']=="GET"){
    $action=filter_var($_GET['action'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
  
    //Pour test http://localhost/AJAXexo/miniAPI/?action=clients 
    if($action=="clients")
    {
        $clientManager=new ClientManager($lePDO);
        $lesObjetsClients=$clientManager->fetchAllClient();
        //Attention : si les attributs de la classe sont private 
        // il que la classe Client implement une interface 
        // et redefinir dans la classe la méthode jsonSerialize
        $jsonClients=json_encode($lesObjetsClients);
        //Indique au navigateur que on affiche du json (n'est pas obligatoire)
        header('Content-Type: application/json');
        
        //On affiche le json
        echo($jsonClients);

    }
}

    


?>